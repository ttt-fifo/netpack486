#########################
# Source for netpack486 #
#########################

Some of the source is downloaded from Slackware mirrors and slackbuilds.org.
The build scripts are changed to make netpack486 possible.
The rest of the Slackware packages, which do not appear in soruce/ directory
are downloaded in binary form from the Slackware mirrors.
