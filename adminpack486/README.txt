################
# adminpack486 #
################

Utilities and network tools for System Administrators

NOTE: this software needs /dev/pts to be mounted in chroot. See howto.txt for
      further explanation.


Packages
========

openssh
-------
Purpose: SSH terminal connections
Version: 8.4p1
Size: 5MB
Help https://www.openssh.com/manual.html

telnet
------
Purpose: Used for network debugging, plain text protocols connections
Version: 0.17
Size: 500KB
Help http:/telnet.org

openssl
-------
Purpose: Command line openssl tools
Version: 1.1.1i
Size: 1MB
Help https://www.feistyduck.com/library/openssl-cookbook/online/

stunnel
-------
Purpose: Tunneling various plain text protocols over TLS/SSL
Version: 5.57
Size: 1MB
Help https://www.stunnel.org/docs.html
NOTE: Take a look at docs/tunneling_guide.txt for some tips and tricks.

curl
----
Purpose: Command line tool for various network protocols
Version: 7.74.0
Size: 1MB
Help https://curl.se/docs/httpscripting.html
