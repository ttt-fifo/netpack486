###############
# chatpack486 #
###############

Chat with friends using IRC and XMPP (formerly jabber)


Packages
========

irssi
-----
Purpose: IRC client
Version: 1.2.2
Size: 1MB
Help https://irssi.org/documentation/
Quickstart: See online manual how to configure. Start by issuing 'irssi' on
the command line and then write '/help' for further information. Quitting is
by writing '/quit'

profanity
---------
Purpose: XMPP (also known as jabber) client
Version: 0.4.6
Size: 1MB
Help https://profanity-im.github.io/userguide.html
Quickstart: See user guide online about how to configure. The interface is
similar to irssi. Start 'profanity', then '/help', '/quit'. Note that this
version works only with UTF-8 console support (otherwise cannot get input from
the keyboard). See howto.txt about how to configure the console.
