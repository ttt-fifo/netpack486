#!/usr/bin/env python
"""
Install netpack486.

Installer for netpack486.

Copyright (c) 2021, Todor Todorov <ttodorov@null.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import curses
import curses.panel
import time
import pwd
import grp
import os
import stat
import shutil

# the import below is working for python 2.3 (no subprocess), 2.x and 3.x
try:
    from subprocess import Popen, PIPE
except ImportError:
    # a quick and dirty Popen, PIPE functionality
    PIPE = None
    class Popen(object):
        def __init__(self, cmd, stdout=PIPE, stderr=PIPE):
            self.cmd = cmd
        def communicate(self):
            stdin, stdout, stderr = os.popen3(self.cmd)
            stdin.close()
            str_stdout = stdout.read()
            str_stderr = stderr.read()
            stdout.close()
            stderr.close()
            return str_stdout, str_stderr


# these documentation directories will be deleted from chroot-----------------
DOCDIRS = [
    "usr/doc",
    "usr/man",
    "usr/local/man",
    "var/man",
    "usr/info",
    "usr/include",
    ]
#-----------------------------------------------------------------------------


#---- ncurses styles ----------------------------------------------------------
BACK = 1
BACKTITLE = 2
FORE = 3
FORESHADE = 4
FORECHOICE = 5
FORENOCHOICE = 6
FORETITLE = 7


def styles():
    """Init ncurses color pairs."""
    curses.init_pair(BACK, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(BACKTITLE, curses.COLOR_RED, curses.COLOR_BLUE)
    curses.init_pair(FORE, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(FORESHADE, curses.COLOR_WHITE, curses.COLOR_CYAN)
    curses.init_pair(FORECHOICE, curses.COLOR_WHITE, curses.COLOR_BLUE)
    curses.init_pair(FORENOCHOICE, curses.COLOR_BLACK, curses.COLOR_CYAN)
    curses.init_pair(FORETITLE, curses.COLOR_RED, curses.COLOR_CYAN)
#---- ncurses styles END-------------------------------------------------------


#---- installation helpers-----------------------------------------------------
def is_installed(pkg, root):
    """Check if package is installed into the given root."""
    pkg_list = pkg.split("/")
    pkg_stem = '.'.join(pkg_list[-1].split('.')[:-1])
    for path in os.listdir("%s/var/adm/packages" % root):
        path_stem = path.split("/")[-1]
        if pkg_stem == path_stem:
            return True
    return False

def installpkg(pkg, root):
    """
    Invoke installpkg for given package and installation root.
    """
    cmd = ['../tools/installpkg', '--root', root, pkg]
    proc = Popen(cmd, stdout=PIPE, stderr=PIPE)
    stdout, stderr = proc.communicate()
    if stderr:
        raise RuntimeError(stderr)


def findpkg(pkgspec):
    """Find a slackware package by given package specification."""
    pkgspec_list = pkgspec.split(os.sep)
    pkgstem = pkgspec_list[-1]
    pkgdir = os.sep.join(pkgspec_list[:-1])
    for filename in os.listdir(pkgdir):
        if filename.startswith(pkgstem) and (filename[-3] == 't') and \
                (filename[-1] == 'z'):
            return "%s%s%s" % (pkgdir, os.sep, filename)

    raise RuntimeError("Missing package: %s.t?z" % pkgspec)

#---- installation helpers END-------------------------------------------------


#---- user interface-----------------------------------------------------------
class BackWin(object):
    """Create background window object."""

    def __init__(self, scr):
        """Initialize."""
        self.scr = scr
        maxy, maxx = self.scr.getmaxyx()
        # the background window steps in from physical screen borders
        self.width = maxx - 2
        self.height = maxy - 2
        self.win = self.scr.subwin(self.height, self.width, 1, 1)
        # the text scrolls whenever reaches lower border
        self.win.scrollok(True)
        self.win.bkgd(' ', curses.color_pair(BACK))
        # create into a panel
        self.panel = curses.panel.new_panel(self.win)

        # display on actual screen
        curses.panel.update_panels()
        self.scr.refresh()

    def addline(self, line, title=False, end="\n"):
        """
        Add one line to background window.

        title: styles the line as title
        end: the end character of the line (similar to python3 print())
        """
        if title:
            self.win.attron(curses.color_pair(BACKTITLE))
        self.win.addstr("%s" % line[:self.width])
        if end:
            self.win.addch(end)
        if title:
            self.win.attroff(curses.color_pair(BACKTITLE))

        curses.panel.update_panels()
        self.scr.refresh()

    def replaceline(self, line, title=False, end="\n"):
        """
        Replace the current line under cursor.

        title: styles the line as a title
        end: the end character (see python3 print())
        """
        # go to beginning of current line and clear the line
        current_y, current_x = self.win.getyx()
        self.win.move(current_y, 0)
        self.win.clrtoeol()
        # add the new content
        self.addline(line, title, end)

    def any_key(self):
        """Wait for user to press any key."""
        self.win.getch()

    def delobj(self):
        """Delete the current object."""
        del self.win
        del self.panel
        curses.panel.update_panels()
        self.scr.clear()
        self.scr.refresh()


class InfoWin(object):
    """Create informational window object."""

    def __init__(self, scr, title, text):
        """
        Initialize.

        scr: the screen to draw on top (usually stdscr)
        title: the window title (str)
        text: the text lines (list of str)
        """
        self.scr = scr
        self.title = title
        self.text = text
        # fixed width, height
        self.width = 50
        self.height = 15
        maxy, maxx = self.scr.getmaxyx()
        # centered and slidely up with 2 rows
        self.starty = int((maxy - self.height)/2) - 2
        self.startx = int((maxx - self.width)/2)
        self.win = self.scr.subwin(self.height, self.width,
                                   self.starty, self.startx)
        self.win.bkgd(' ', curses.color_pair(FORE))
        # draw all
        self.draw_text()
        self.draw_borders()
        # create into panel
        self.panel = curses.panel.new_panel(self.win)
        #actual display
        self.display()

    def display(self):
        """Display on physical screen."""
        curses.panel.update_panels()
        self.scr.refresh()

    def draw_text(self):
        """Draw the text into the window."""
        self.win.attron(curses.color_pair(FORETITLE))
        self.win.addstr(1, 1, self.title)
        self.win.attroff(curses.color_pair(FORETITLE))

        for i in range(0, len(self.text)):
            self.win.addstr(2 + i, 1, self.text[i][:self.width -2])

    def draw_borders(self):
        """Draw the borders."""
        # the shadow lines
        self.win.attron(curses.color_pair(FORESHADE) | curses.A_BOLD)
        self.win.addch(0, 0, curses.ACS_ULCORNER)
        self.win.hline(0, 1, curses.ACS_HLINE, self.width - 2)
        self.win.vline(1, 0, curses.ACS_VLINE, self.height - 3)
        self.win.addch(self.height - 2, 0, curses.ACS_LLCORNER)
        self.win.attroff(curses.color_pair(FORESHADE) | curses.A_BOLD)

        # the foreground lines
        self.win.attron(curses.color_pair(FORE) | curses.A_BOLD)
        self.win.hline(self.height - 2, 1, curses.ACS_HLINE, self.width - 2)
        # famous ncurses bug cannot write lower right (so one row up)
        self.win.addch(self.height - 2, self.width - 1, curses.ACS_LRCORNER)
        self.win.vline(1, self.width - 1, curses.ACS_VLINE, self.height - 3)
        self.win.addch(0, self.width - 1, curses.ACS_URCORNER)
        self.win.attroff(curses.color_pair(FORE) | curses.A_BOLD)

    def delobj(self):
        """Delete the window object."""
        del self.win
        del self.panel
        curses.panel.update_panels()
        self.scr.clear()
        self.scr.refresh()

    def any_key(self):
        """Wait for the user to press any key."""
        self.win.getch()


class DialogWin(InfoWin):
    """Inherit from informational window, create dialog object"""

    def __init__(self, scr, title, text, yes=True):
        """
        Initialize.

        scr: the screen to draw on top (usually stdscr)
        title: the window title (str)
        text: the text lines (list of str)
        yes: is yes button highlighted? True, False
        """
        super(DialogWin, self).__init__(scr, title, text)

        self.yes = yes

        self.btn_width = self.width
        self.btn_height = 2
        # 1 row up into informational window due to famous curses bug...
        self.btn_starty = self.starty + self.height - 1
        self.btn_startx = self.startx
        self.btn_win = self.scr.subwin(self.btn_height, self.btn_width,
                                       self.btn_starty, self.btn_startx)
        self.btn_win.bkgd(' ', curses.color_pair(FORE))
        # accepts keypad characters
        self.btn_win.keypad(1)
        # draw buttons only (the rest is already drawn)
        self.draw_btn()
        # create into own panel (yes, one more for buttons only)
        self.btn_panel = curses.panel.new_panel(self.btn_win)
        # display second time (some performance loss)
        self.display()
        
    def draw_btn(self):
        """Draw the buttons."""
        # Yes button
        if self.yes:
            styl = curses.color_pair(FORECHOICE) | curses.A_BOLD
        else:
            styl = curses.color_pair(FORENOCHOICE)
        self.btn_win.attron(styl)
        self.btn_win.addstr(0, 2, "  Yes  ")
        self.btn_win.attroff(styl)

        # No button
        if self.yes:
            styl = curses.color_pair(FORENOCHOICE)
        else:
            styl = curses.color_pair(FORECHOICE) | curses.A_BOLD
        self.btn_win.attron(styl)
        self.btn_win.addstr(0, 9, "   No  ")
        self.btn_win.attroff(styl)

    def get_user_input(self):
        """Get input from user."""
        while True:
            ch = self.btn_win.getch()
            if ch == curses.KEY_LEFT:
                self.yes = True
            elif ch == curses.KEY_RIGHT:
                self.yes = False
            elif (ch == curses.KEY_ENTER) or (ch == ord('\n')) \
                 or (ch == ord(' ')):
                return self.yes
            elif (ch == ord('Y')) or (ch == ord('y')):
                self.yes = True
                return self.yes
            elif (ch == ord('N')) or (ch == ord('n')) or (ch == ord('q')) \
                 or (ch == ord('Q')):
                self.yes = False
                return self.yes

            # redraw new state on screen
            self.draw_btn()
            self.scr.refresh()

    def delobj(self):
        """Delete the window object."""
        del self.btn_win
        del self.btn_panel
        super(DialogWin, self).delobj()
#---- user interface END-------------------------------------------------------


def main(scr, *args):
    """
    Execute main installation cadance.
    """
    # initialize curses and stdscr
    styles()
    scr.bkgd(' ', curses.color_pair(BACK))
    curses.curs_set(False)

    if os.geteuid() != 0:
        raise RuntimeError("\n#########################\n"
                           "You need root privileges!\n"
                           "#########################")

    try:
        user = pwd.getpwnam('netpack')
    except KeyError:
        raise RuntimeError("\n##########################\n"
                           "Install basepack486 first!\n"
                           "##########################")

    if not os.path.isfile("%s/donttouchme" % user.pw_dir):
        raise RuntimeError("\n##########################\n"
                           "Install basepack486 first!\n"
                           "##########################")

    title = "                Install 'links'?"
    text = [
        " [about 2MB]",
        " ",
        " ",
        " Links is a console web browser.",
        " ",
        " This version is capable of HTTP/HTTPS.",
        " ",
        " Compiled without any graphics support.",
        " ",
        " Website http://links.twibright.com/",
        ]
    fore = DialogWin(scr, title, text)
    install_links = fore.get_user_input()
    fore.delobj()

    title = "                Install 'wget'?"
    text = [
        " [about 9MB]",
        " ",
        " ",
        " ",
        " Wget is a command line utility for retrieving",
        " files using HTTP, HTTPS, FTP and FTPS.",
        " ",
        " ",
        " ",
        " Website https://www.gnu.org/software/wget/",
        ]
    fore = DialogWin(scr, title, text)
    install_wget = fore.get_user_input()
    fore.delobj()

    if (not install_links) and (not install_wget):
        title = "               Nothing to install..."
        text = [
            " ",
            " ",
            " ",
            " ",
            " ",
            "               Press any key...",
        ]
        fore = InfoWin(scr, title, text)
        fore.any_key()
        return False

    back = BackWin(scr)

    back.addline("Installing packages:", title=True)
    if install_links:
        listfd = open("./lists/links.txt", "r")
        for line in listfd.readlines():
            line = line.strip()
            pkg = findpkg(line)
            back.addline("[ ] %s" % pkg, end="")
            if is_installed(pkg, user.pw_dir):
                back.replaceline("[*] %s" % pkg)
            else:
                installpkg(pkg, user.pw_dir)
                back.replaceline("[V] %s" % pkg)
        listfd.close()
    if install_wget:
        listfd = open("./lists/wget.txt", "r")
        for line in listfd.readlines():
            line = line.strip()
            pkg = findpkg(line)
            back.addline("[ ] %s" % pkg, end="")
            if is_installed(pkg, user.pw_dir):
                back.replaceline("[*] %s" % pkg)
            else:
                installpkg(pkg, user.pw_dir)
                back.replaceline("[V] %s" % pkg)
        listfd.close()

    # delete docs/includes
    back.addline("Deleting unneeded directories:", title=True)
    for docdir in DOCDIRS:
        docdir = user.pw_dir + '/' + docdir
        back.addline('[ ] %s' % docdir, end="")
        shutil.rmtree(docdir, ignore_errors=True)
        back.replaceline('[X] %s' % docdir)

    # copy README
    back.addline("Copying README:", title=True)
    back.addline(
        '[ ] cp README.txt %s/home/netpack/README.webpack486' % user.pw_dir,
        end="")
    shutil.copyfile("./README.txt",
                    "%s/home/netpack/README.webpack486" % user.pw_dir)
    os.chown("%s/home/netpack/README.webpack486" % user.pw_dir,
             user.pw_uid, user.pw_gid)
    back.replaceline(
        '[V] cp README.txt %s/home/netpack/README.webpack486' % user.pw_dir)

    # done with the background window, wait for any key
    back.addline("Done! Happy browsing! Press any key...", title=True)
    back.any_key()
    back.delobj()


if __name__ == "__main__":
    curses.wrapper(main)
