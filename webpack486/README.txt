##############
# webpack486 #
##############

Browsing Internet and downloading files and mirroring sites


Packages
========

links
-----
Purpose: Web browser
Version: 2.2.1
Size: 2MB
Help http://links.twibright.com
Quickstart: When started a terminal interface appears. To open the menu press
'ESC' once. To open new website, press 'g'.

wget
----
Purpose: Web downloader and mirroring sites
Version: 1.20.3
Size: 9MB
Help https://www.gnu.org/software/wget/manual/wget.html
Quickstart: With wget you can download files and mirror sites using HTTP(S) and
FTP protocols. Example usage

$ wget https://www.gnu.org/software/wget/manual/wget.html
