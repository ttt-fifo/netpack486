######################################
# netpack486 installed on CentOS 4.3 #
######################################

-------------------------------------------------------------------------------
Kernel: 2.6.9-34.EL
-------------------------------------------------------------------------------
.txz packages: not on my system. Suppose they can be installed from rpm, but
               I used converted packages to .tgz (see howto.txt)
-------------------------------------------------------------------------------
Python: 2.3.4
-------------------------------------------------------------------------------
install.py: working. I had some problems with the terminal - curses not
            displaying properly. I suppose it is about my terminal.
            Also needed to add group 'shadow' on the system prior running
            install.py (needed for netpack)
-------------------------------------------------------------------------------
tools/installpkg: working
-------------------------------------------------------------------------------
su installed from basepack486: yes
-------------------------------------------------------------------------------
chroot --userspec: absent. Logging in as root into chroot and then:
                   $ su - netpack
-------------------------------------------------------------------------------
/usr/bin/netpack: not working on my system. I do not have 'mountpoint' command
                  on my system. Suppose it may be installed via rpm. I cleared
                  all mounting stuff from /usr/bin/netpack and configured
                  /etc/fstab to mount the chroot binds at startup.
                  (see howto.txt)
-------------------------------------------------------------------------------
Packs installed: basepack486, webpack486, mailpack486, chatpack486, bbspack486,
                 adminpack486.
                 Most of the software is working.
-------------------------------------------------------------------------------
Problems encountered:
* curses broken somehow during install.py
* qodem cannot display ANSI terminals properly.
--> I suppose the both problems are connected with some misconfiguration on my
    terminal.
* needed to add group 'shadow' on the system prior running install.py
-------------------------------------------------------------------------------
