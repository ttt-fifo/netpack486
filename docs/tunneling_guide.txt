##############################
# netpack486 tunneling guide #
##############################

Copyright (c) 2021, Todor Todorov <ttodorov@null.net>
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


TOC
===
* What you can do with netpack486 tunneling?
* Which protocols can be tunneled?
* Quickstart on stunnel tunneling
* How to authenticate the server while tunneling?
* Get server certificate manually



What you can do with netpack486 tunneling?
==========================================
Imagine you have some old network application installed with your old Linux
distro. It cannot connect via SSL/TLS anymore, because it uses old version of
the protocols. Then you can configure a tunnel into the netpack486, converting
the plain text protocol from your old network application into SSL/TLS
encrypted protocol. How it works:


 Your old Linux          |               |
 distro                  |               |
                         |               |              +-----------------+
 +---------+             |  +---------+  |              |                 |
 | old app |--------------->| stunnel |---------------->| Internet server |
 +---------+  plaintext  |  +---------+  |   SSL/TLS    |                 |
              network    |               |   network    +-----------------+
              protocol   |               |   protocol
                         | netpack486    |
                         | chroot        |



Which protocols can be tunneled?
================================
Some protocols are suitable / possible for tunneling, others are not. Here is
my experience:

* IRC(S): Working flawlessly. Just point stunnel to the SSL port of the IRC
  server (usually port 6697) and configure stunnel to listen at 6667 port. Any
  IRC client will connect via plain text IRC protocol to your localhost:6667.
* NTTP(S): News protocol. Working.
* XMPP(S): Usually not working. XMPP uses STARTTLS, which means the client
  connects plain text all the way to the server at startup and then negotiates
  TLS. I suceed to tunnel STARTLS with more complex setup, but netpack486
  is not suitable for such setup.
* IMAP(S): I think it works, but untested. Please write me back in case you
  suceed.
* POP(S): I think it works, but untested. Please write me back in case you
  suceed.
* SMTP(S): The same. I thing it works if there is not STARTTLS enabled. Please
  write me back if suceed.
* HTTP(S): Not working. You would need a full featured proxy (e.g. squid)
  in order to setup web browsing, which is not the intention of current guide.

--> Please write me back if you have comments on other protocols and I will
    include them here.


Quickstart on stunnel tunneling
===============================

* Login into netpack486 chroot environment and create stunnel configuration
  file about your desired connection (here example for an IRC server):

netpack@darkstar[chroot]~$ vi myirctunnel.conf
#------------------------------------------------------------------------------
foreground = yes
pid =

[choose_a_name_for_the_connection]
client = yes
accept = 127.0.0.1:6667
connect = fqdn.of.your.irc.server:6669
#------------------------------------------------------------------------------
--> save the file myirctunnel.conf

* Then start stunnel with the configuration above. You will see something like:

netpack@darkstar[chroot]~$ stunnel myirctunnel.conf 
08:04:57 LOG5[ui]: stunnel 5.57 on i486-slackware-linux-gnu platform
08:04:57 LOG5[ui]: Compiled/running with OpenSSL 1.1.1i  8 Dec 2020
08:04:57 LOG5[ui]: Threading:PTHREAD Sockets:POLL,IPv6 TLS:ENGINE,FIPS,OCSP,
PSK,SNI Auth:LIBWRAP
08:04:57 LOG5[ui]: Reading configuration from file /home/netpack/myirctunnel.co
nf
08:04:57 LOG5[ui]: UTF-8 byte order mark not detected
08:04:57 LOG5[ui]: FIPS mode disabled
08:04:58 LOG4[ui]: Service [choose_a_name_for_the_connection] needs authenticat
ion to prevent MITM attacks
08:04:58 LOG5[ui]: Configuration successful

* Leave the current window like this. Open another tty (or xterminal, or
  whatever console). Do not login into the chrooted environment. Make a test
  connection using telnet to see if IRC server responds:

(it depends on the server, you may need to wait up to 20-30sec. Some other
 servers never respond...)

$ telnet 127.0.0.1 6667
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.

NOTICE AUTH :*** Processing connection to irc.XXXXX.XXX
NOTICE AUTH :*** Looking up your hostname...
NOTICE AUTH :*** Couldn't look up your hostname

--> The server responded. You may end the connection with ctr+]

* Some tips how to debug in case the connection is not working:
  - instead of telnet you may use command 'nc' (netcat) if available on your
    system
  - you may add the line 'debug = 7' close to the beginning of myirctunnel.conf
    This will give you verbose output.
  - you may press few 'ENTER's after starting the telnet command. This
    'wakes up' some IRC servers :)

* Now you can configure your old IRC application to connect to 127.0.0.1:6667
  without encryption and it should work all the way up to the IRC server.


How to authenticate the server while tunneling?
===============================================
This is one of the possible methods. There are many more options described in
the stunnel manual. See the stunnel manual for details.

* Put these new authentication options in your myirctunnel.conf:
#------------------------------------------------------------------------------
## this is the server certificate see below how to obtain
CAfile = server_cert.pem
## verify 4 tells stunnel to check only server_cert.pem and ommit all other
## certificate checks
verify = 4
foreground = yes
pid =

[choose_a_name_for_the_connection]
client = yes
accept = 127.0.0.1:6667
connect = fqdn.of.your.irc.server:6669
#------------------------------------------------------------------------------

* Now you need to get the server certificate manually into server_cert.pem
  See next section.


Get server certificate manually
===============================
!NOTE! this instruction describes how to get the server certificate, but it
does not check the authenticy of the sertificate. Use your imagination how
to check manually if this certificate really belongs to the server...

* Use the command openssl to obtain the server certificate via network

(this must be issued inside netpack586 chroot in order to use the newer
 openssl protocol version while connecting)

netpack@darkstar[chroot]~$ openssl s_client -connect <server_name>:<port>

* This will output much connection information, but the certificate you need
  to place in server_cert.pem is the text between these two lines (including
  the two lines):

-----BEGIN CERTIFICATE-----
....
here some crypted ASCII lines
....
-----END CERTIFICATE-----
