# NetPack486

Micro Linux, which you can chroot into your old distribution to resurrect the SSL/TLS connectivity.

## Features

* Newer OpenSSL (v1.1.1i)
* Includes terminal network clients
* Compiled for 486 and newer processors
* Working with diverse Linux distributions and versions
* Running on top of kernel 2.6.0 or newer
* Python curses installer(s). Install only the modules you need, called 'packs'.

## Motivation

The major Linux distributions are dropping the support of the older hardware. The users of 486 and old Pentium are stuck with unsupported versions of their Linux distributions. The SSL/TLS connections are impossible for them anymore and their Linux boxes become hooked off the Internet. Here comes NetPack486 - by chrooting it into the old Linux boxes they receive fresh network stack and software capable of SSL/TLS connections.

## SECURITY DISCLAIMER

I do not claim that this project adds some more security to the existing systems, neither it follows the best security practices. The aim of the current project is not more than adding usability to the old OSes and allowing connections to Internet using newer SSL/TLS protocol versions.

## Quickstart

### Prerequisites

Kernel 2.6.0 or newer (will not run on 2.4). 32bit system (most probably runs on 64bit, untested). Install scripts require Python 2.3 or newer (there are alternate [instructions](docs/manual_install.txt) how to install manually). xz compression utility - in case not present there is [instruction](docs/howto.txt) how to convert all packages to gz compression prior installation. Your 'chroot' command to eventually implement the '--userspec' switch, but there are alternate [instructions](docs/howto.txt) how to configure without '--userspec'. Be sure to review [docs/howto.txt](docs/howto.txt) first in order to know what issues you may encounter during installation and usage.

### Install

* On another Linux/UNIX box, capable of networking, download the netpack486:

```
$ git clone https://gitlab.com/ttt-fifo/netpack486
```

* Copy netpack486 installation to the target box using your means of external storage - USB, CDROM, external HDD, etc.

* Add the user 'netpack' on the target box. Your mileage may vary, but generic command for adding an user is:

```
# useradd -m netpack
```

* Install basepack486 - this is the base chroot system. Look at README prior installing:

```
# cd basepack486
# less README.txt
# ./install.py
```

* Install the desired software from `*pack486` directories by changing to each directory and starting `./install.py`. Be sure to read each README.txt first, because there are info and instructions about the current pack. For example:

```
# cd webpack486
# less README.txt
# ./install.py
```

* Start netpack and login into the chroot:

```
# /usr/bin/netpack
```

* Consult [docs/howto.txt](docs/howto.txt) about any encountered problems. Report a [bug](https://gitlab.com/ttt-fifo/netpack486/-/issues) in case your issue is not listed there.


## List of packs

* basepack486: base chroot system with some utilities for managing the environment
* webpack486: browsing websites and downloading files
* mailpack486: mail user agent and tools
* chatpack486: chat over the IRC and XMPP (formerly jabber) networks
* bbspack486: connect to BBS servers. ANSI capable display, telnet and ssh connections
* adminpack486: sysadmin network utilities. Also look at [docs/tunneling_guide.txt](docs/tunneling_guide.txt) for tips how to tunnel your old network software.

## System list

The list of Linux distributions tested with netpack486. Please write me an e-mail if suceed to run on top of some additional distro.

* Slackware 13.37 - see [report](docs/system_slackware-13.37.txt)
* Slackware 12.2 - see [report](docs/system_slackware-12.2.txt)
* PsychOS486 1.6.5 - see [report](docs/system_psychos486-1.6.5.txt) and installation [instruction](docs/system_psychos486-1.6.5_install.txt)
* Debian 3 (sarge) - see [report](docs/system_debian-31r8-i386_sarge.txt)
* Debian 4 (etch) - see [report](docs/system_debian-40r7-i386_etch.txt)
* Mint 8 - see [report](docs/system_mint-8.txt)
* CentOS 4.3 - see [report](docs/system_centos-4.3.txt)

## License

All the work on the current project is licensed under BSD 2-Clause License. The other scripts and packages are copyrighted under their individual licenses described into their source code. For detailed information look at the [LICENSE](LICENSE) file.

## Acknowledgments

* The [Slackware Linux Project](http:/slackware.org) for their excelent work ongoing more than two and a half decades. netpack486 is built on top of their SlackBuilds and packages taken from Slackware 13.37 and other versions.
* [slackbuilds.org](https://slackbuilds.org) - some SlackBuild scripts are taken or consulted from there.
* [TheOuterLinux](https://theouterlinux.gitlab.io) for giving me some directions about Linux on old hardware.

## Bugs and issues

You can report bugs and issues at the gitlab form [here](https://gitlab.com/ttt-fifo/netpack486/-/issues) or send me email directly: <ttodorov@null.net>

## Author

Todor Todorov <ttodorov@null.net>

## Other projects

You may be also interested in these projects:

* [PsychOS486 Linux](https://psychoslinux.gitlab.io/486/index.html)
* [Make the 486 Great Again!](https://yeokhengmeng.com/2018/01/make-the-486-great-again/)
