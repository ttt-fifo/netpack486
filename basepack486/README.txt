###############
# basepack486 #
###############

The base chroot system for netpack486 with some tools for managing
the chroot environment.


Packages
========

Base tools
----------
Purpose: Base system and utilities/tools
Version: Slackware 13.37
Size: 54MB
Help https://www.slackbook.org/
Quickstart: To login into the chrooted system use the startup script
/usr/bin/netpack from outside the chroot. After successfull login you can check
the network with ping or tracepath. Base tools include the editor vi and the
other utilities which one may expect from a Linux shell: less, ls, etc.

su
--
Purpose: Changing user privileges
Version: coreutils-8.15
Size: 2MB
Help https://www.gnu.org/software/coreutils/manual/
Quickstart: You need 'su' command into the chroot in case the 'chroot' command
of your system does not support --userspec switch. Then you will always be
login into the chroot as root and then issuing the command to lower the
user privileges:

# su - netpack
