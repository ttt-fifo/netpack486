##############
# bbspack486 #
##############

Connect to BBS systems

NOTE: this software needs /dev/pts to be mounted in chroot. See howto.txt for
      further explanation. In case no /dev/pts in chroot qodem will not be able
      to open connections.


Packages
========

qodem
-----
Purpose: BBS connect software
Version: 1.0.0
Size: 2MB
Help http://qodem.sourceforge.net/getting_started/getting_started.html
Quickstart: After the first start it will create a sample configuration into
~/.qodem directory. Review the configuration files there, they are
self-explanatory. Capable of displaying ANSI, capable of x,y,z-modem file
transfer. Connections are telnet and ssh. ssh is done via external program,
so you need to install openssh if you want ssh connections. 

openssh
-------
Purpose: SSH terminal connections
Version: 8.4p1
Size: 5MB
Help https://www.openssh.com/manual.html
Quickstart: The current purpose is to be started from qodem, but if you need
to start it manually, the usual command line is:

$ ssh fqdn.of.a.server.com
