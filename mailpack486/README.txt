###############
# mailpack486 #
###############

Mail User Agent and utilities


Packages
========

mutt
----
Purpose: Mail User Agent
Version: 2.0.4
Size: 4MB
Help https://www.mutt.org/doc/manual
Quickstart: Mutt has multiple configuration options. See the online manual how
to configure. Start by issuing 'mutt' on command line. The quit command is 'q'
while in the mutt interface.

gnupg
-----
Purpose: Encrypt and sign your messages
Version: 1.4.23
Size: 5MB
Help https://gnupg.org/documentation/manuals.html
Quickstart: GnuPG can encrypt the text you are sending via e-mail so only
the recipient can read it. Here is example how you can encrypt a text from the
command line and protect it with a password:

$ gpg --symmetric --armor
Enter passphrase: 
Repeat passphrase:
text to encrypt    
-----BEGIN PGP MESSAGE-----

jA0EBwMCNjCi8+or0ZNg0kUBhE67XSqVBGPOJYcy2VEZO/hj02kVWLqtrRgRcl8A
UXAhHVgqbyjGK9fI0Qv9dSKmQ2IwhuyscOtdkYlG3qjwG5E9KC4=
=z6uu
-----END PGP MESSAGE-----

--> First prompts you for a password, then waits for your text to encrypt. End
    the text with [Enter] Ctr+d and you will receive the encrypted version of
    the message
